package com.jbpark.parkj.layout_test;

/**
 * Created by parkj on 6/1/2015.
 */
public interface DownloadFinishedListener {
    public void createTab();
}
