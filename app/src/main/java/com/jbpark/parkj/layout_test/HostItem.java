package com.jbpark.parkj.layout_test;

import android.content.Intent;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by parkj on 6/2/2015.
 */
public class HostItem {

    public static final String ITEM_SEP = System.getProperty("line.separator");

    public final static String USERNAME = "username";
    public final static String TITLE = "title";
    public final static String DESCRIPTION = "description";
    public final static String DATE = "date";
    public final static String LOCATION = "location";
    public final static String FILENAME = "filename";

    public final static SimpleDateFormat FORMAT = new SimpleDateFormat(
            "yyyy-MM-dd HH:mm:ss", Locale.CANADA);

    private String mUsername = new String();
    private String mTitle = new String();
    private String mDescription = new String();
    private String mLocation = new String();
    private Date mDate = new Date();

    HostItem(String username, String title, String description, String location, Date date) {
        this.mUsername = username;
        this.mTitle = title;
        this.mDescription = description;
        this.mLocation = location;
        this.mDate = date;
    }

    public String getUsername() {return mUsername;}

    public void setUsername(String username) {mUsername = username;}

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public String getLocation() {
        return mLocation;
    }

    public void setLocation(String location) {
        mLocation = location;
    }

    public Date getDate() {
        return mDate;
    }

    public void setDate(Date date) {
        mDate = date;
    }

    public static void packageIntent(Intent intent, String username, String title,
                                     String description, String location, String date) {

        intent.putExtra(HostItem.USERNAME, username);
        intent.putExtra(HostItem.TITLE, title);
        intent.putExtra(HostItem.DESCRIPTION, description);
        intent.putExtra(HostItem.LOCATION, location);
        intent.putExtra(HostItem.DATE, date);

    }

    public String toString() {
        return mUsername + ITEM_SEP + mTitle + ITEM_SEP + mDescription + ITEM_SEP
                + mLocation + ITEM_SEP + FORMAT.format(mDate);
    }


}
