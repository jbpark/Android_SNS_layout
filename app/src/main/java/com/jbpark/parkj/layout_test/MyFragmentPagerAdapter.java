package com.jbpark.parkj.layout_test;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class MyFragmentPagerAdapter extends FragmentPagerAdapter{

    final int PAGE_COUNT = 4;
    private final Context mContext;
    private int mPosition = 0;

    /** Constructor of the class */
    public MyFragmentPagerAdapter(FragmentManager fm, Context context, int position) {
        super(fm);
        mContext = context;
        this.mPosition = position;
    }

    /** This method will be invoked when a page is requested to create */
    @Override
    public Fragment getItem(int arg0) {
        Bundle data = new Bundle();
        switch(arg0){
            case 0:
                Feeds_Fragment feedsFragment = new Feeds_Fragment();
                return feedsFragment;
            case 1:
                My_Host_Items_Fragment publicFragment = new My_Host_Items_Fragment();
                return publicFragment;
            case 2:
                Favorite_Fragment favFragment = new Favorite_Fragment();
                return favFragment;
            case 3:
                Setting_Fragment settingFragment = new Setting_Fragment();
                return settingFragment;
        }
        return null;
    }

    public void setPosition(int position){
        this.mPosition = position;
    }

    public int getPosition(){
        return mPosition;
    }

    /** Returns the number of pages */
    @Override
    public int getCount() {
        return PAGE_COUNT;
    }
}