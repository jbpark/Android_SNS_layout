package com.jbpark.parkj.layout_test;


import android.app.Activity;
import android.app.DialogFragment;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

public class My_Host_Items_Fragment extends Fragment {

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.my_host_items, null);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ListView lv_my_hosts = (ListView) getActivity().findViewById(R.id.lv_my_hosts);
        ColorDrawable divcolor = new ColorDrawable(Color.DKGRAY);
        lv_my_hosts.setDivider(divcolor);
        lv_my_hosts.setDividerHeight(1);
        lv_my_hosts.setAdapter(MainActivity.mMyHostsAdapter);
    }

}