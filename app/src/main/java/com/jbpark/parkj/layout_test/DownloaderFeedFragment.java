package com.jbpark.parkj.layout_test;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;


/**
 * Created by parkj on 6/1/2015.
 */
public class DownloaderFeedFragment extends Fragment {

    private Context mContext;
    private DownloadFinishedListener mCallback;
    private static final String TAG = "DownloaderTaskFragment";

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        mContext = activity.getApplicationContext();
        try {
            mCallback = (DownloadFinishedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement DownloadFinishedListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        DownloaderTask downloaderTask = new DownloaderTask();
        downloaderTask.execute();

        //Download
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallback = null;
    }

    public class DownloaderTask extends AsyncTask<Void, Void, Integer>
    {

        @Override
        protected Integer doInBackground(Void... params) {
            downloadFeeds();
            return 0;
        }

        @Override
        protected void onPostExecute(Integer param){
            if (mCallback != null){
                mCallback.createTab();
            }
        }
    }

    private void downloadFeeds() {
        final int simulatedDelay = 5000;
        try {
            // Pretend downloading takes a long time
            Thread.sleep(simulatedDelay);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
