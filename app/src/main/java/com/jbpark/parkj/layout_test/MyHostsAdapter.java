package com.jbpark.parkj.layout_test;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by parkj on 6/2/2015.
 */
public class MyHostsAdapter extends BaseAdapter {

    private final List<HostItem> mItems = new ArrayList<HostItem>();
    private final Context mContext;

    public MyHostsAdapter(Context context) {
        mContext = context;
    }

    public void add(HostItem item) {
        mItems.add(item);
        notifyDataSetChanged();
    }

    public void clear() {
        mItems.clear();
        notifyDataSetChanged();
    }


    public void removeItem(int pos) {
        mItems.remove(pos);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public Object getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final HostItem hostItem = mItems.get(position);

        RelativeLayout itemLayout = (RelativeLayout) convertView;

        if (convertView == null){
            itemLayout = (RelativeLayout) LayoutInflater
                    .from(mContext)
                    .inflate(R.layout.host_item, parent, false);
        }

        final TextView titleView = (TextView) itemLayout.findViewById(R.id.titleView);
        titleView.setText(hostItem.getTitle());

        final TextView descriptionView = (TextView) itemLayout.findViewById(R.id.descriptionView);
        descriptionView.setText(hostItem.getDescription());

        final TextView locationView = (TextView) itemLayout.findViewById(R.id.locationView);
        locationView.setText(hostItem.getLocation());

        final TextView dateView = (TextView) itemLayout.findViewById(R.id.dateView);
        dateView.setText(hostItem.FORMAT.format(hostItem.getDate()));

        return itemLayout;
    }
}
