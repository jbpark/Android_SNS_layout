package com.jbpark.parkj.layout_test;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

public class Feeds_Fragment extends Fragment {

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.feeds_fragment, null);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ListView lv_feeds = (ListView) getActivity().findViewById(R.id.lv_feeds);
        ColorDrawable divcolor = new ColorDrawable(Color.DKGRAY);
        lv_feeds.setDivider(divcolor);
        lv_feeds.setDividerHeight(1);
        lv_feeds.setAdapter(MainActivity.mFeedAdapter);
    }

}