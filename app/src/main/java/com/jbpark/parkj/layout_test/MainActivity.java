package com.jbpark.parkj.layout_test;

import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.RelativeLayout;


public class MainActivity extends ActionBarActivity implements DownloadFinishedListener {

    private static final String TAG_FEEDS_FRAGMENT = "feeds_fragment";
    private static final String TAG_PUBLIC_FRAGMENT = "public_fragment";
    private static final String TAG_FAVORITE_FRAGMENT = "favorite_fragment";
    private static final String TAG_SETTING_FRAGMENT = "setting_fragment";
    private static final String TAG_DOWNLOADER_FRAGMENT = "downloader_fragment";
    public static final int ADD_HOST_ITEM_REQUEST = 0;

    private ViewPager mPager;
    private ActionBar mActionbar;
    private RelativeLayout background;

    public static MyFragmentPagerAdapter fragmentPagerAdapter;
    public static FeedAdapter mFeedAdapter;
    public static MyHostsAdapter mMyHostsAdapter;
    public static FavoriteAdapter mFavoriteAdapter;

    private DownloaderFeedFragment mDownloaderFragment;
    private FragmentManager mFragmentManager;
    private Feeds_Fragment mFeedsFragment;
    private My_Host_Items_Fragment mPublicFragment;
    private Favorite_Fragment mFavoriteFragment;
    private Setting_Fragment mSettingFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mActionbar = getSupportActionBar();
        mActionbar.hide();

        mFeedAdapter = new FeedAdapter(getApplicationContext());
        mMyHostsAdapter = new MyHostsAdapter(getApplicationContext());
        mFavoriteAdapter = new FavoriteAdapter(getApplicationContext());

        mFragmentManager = getSupportFragmentManager();
        fragmentPagerAdapter = new MyFragmentPagerAdapter(mFragmentManager, getApplicationContext(), 0);

        mPager = (ViewPager) findViewById(R.id.pager);

        background = (RelativeLayout) findViewById(R.id.background);

        installDownloaderTaskFragment();
    }

    private void installDownloaderTaskFragment() {

        mDownloaderFragment = new DownloaderFeedFragment();

        mFragmentManager.beginTransaction()
                .add(mDownloaderFragment, TAG_DOWNLOADER_FRAGMENT).commit();
    }

    public void createTab(){

        mActionbar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        ViewPager.SimpleOnPageChangeListener pageChangeListener = new ViewPager.SimpleOnPageChangeListener(){
            @Override
            public void onPageSelected(int position) {
            mPager.setCurrentItem(position);
            mActionbar.setSelectedNavigationItem(position);
            fragmentPagerAdapter.setPosition(position);
            }
        };

        mPager.setOnPageChangeListener(pageChangeListener);

        mPager.setAdapter(fragmentPagerAdapter);

        mActionbar.setDisplayShowTitleEnabled(true);

        ActionBar.TabListener tabListener = new ActionBar.TabListener() {

            @Override
            public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {
            }

            @Override
            public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {
                mPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {
            }
        };

        ActionBar.Tab tab = mActionbar.newTab()
                .setText("Feeds")
                .setTabListener(tabListener);

        mActionbar.addTab(tab);

        tab = mActionbar.newTab()
                .setText("My Hosts")
                .setTabListener(tabListener);
        mActionbar.addTab(tab);

        tab = mActionbar.newTab()
                .setText("Fav")
                .setTabListener(tabListener);
        mActionbar.addTab(tab);

        tab = mActionbar.newTab()
                .setText("Setting")
                .setTabListener(tabListener);
        mActionbar.addTab(tab);

        background.setBackgroundResource(0);
        background.setBackgroundColor(getResources().getColor(R.color.dark_white));

        mActionbar.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if(id == R.id.add_host_item){
            Intent new_host_item = new Intent(MainActivity.this, Add_Host_Item.class);
            startActivityForResult(new_host_item, ADD_HOST_ITEM_REQUEST);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
