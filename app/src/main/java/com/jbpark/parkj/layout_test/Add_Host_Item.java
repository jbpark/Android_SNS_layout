package com.jbpark.parkj.layout_test;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.doomonafireball.betterpickers.calendardatepicker.CalendarDatePickerDialog;
import org.joda.time.DateTime;

/**
 * Created by parkj on 6/2/2015.
 */
public class Add_Host_Item extends ActionBarActivity implements CalendarDatePickerDialog.OnDateSetListener{

    Button mFromButton;
    Button mToButton;
    CalendarDatePickerDialog mFromDialog;
    CalendarDatePickerDialog mToDialog;
    DateTime dateTime;
    private static final String FRAG_TAG_DATE_PICKER = "fragment_date_picker_name";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_host_item);
        mFromButton = (Button) findViewById(R.id.from_button);
        mToButton = (Button) findViewById(R.id.to_button);
        dateTime = DateTime.now();

        dateTime.getYear();
        mFromButton.setText("" + dateTime.getYear() + "-" + (dateTime.getMonthOfYear()-1) + "-" + dateTime.getDayOfMonth());
        mToButton.setText("" + dateTime.getYear() + "-" + (dateTime.getMonthOfYear()-1) + "-" + dateTime.getDayOfMonth());

        mFromButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getSupportFragmentManager();
                DateTime now = DateTime.now();

                mFromDialog = CalendarDatePickerDialog
                        .newInstance(Add_Host_Item.this, now.getYear(), now.getMonthOfYear() - 1,
                                now.getDayOfMonth());
                mFromDialog.show(fm, FRAG_TAG_DATE_PICKER);
            }
        });


        mToButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getSupportFragmentManager();
                DateTime now = DateTime.now();

                mToDialog = CalendarDatePickerDialog
                        .newInstance(Add_Host_Item.this, now.getYear(), now.getMonthOfYear() - 1,
                                now.getDayOfMonth());
                mToDialog.show(fm, FRAG_TAG_DATE_PICKER);
            }
        });
    }

    @Override
    public void onDateSet(CalendarDatePickerDialog dialog, int year, int monthOfYear, int dayOfMonth) {
        if (dialog == mFromDialog){
            Toast.makeText(getApplicationContext(), "From Dialog: " + year + " " + monthOfYear + " " + dayOfMonth, Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(getApplicationContext(), "To Dialog: " + year + " " + monthOfYear + " " + dayOfMonth, Toast.LENGTH_SHORT).show();
        }

        //date.setText(year + "-" + monthOfYear + "-" + dayOfMonth);
    }

    @Override
    public void onResume() {
        // Example of reattaching to the fragment
        super.onResume();
        CalendarDatePickerDialog calendarDatePickerDialog = (CalendarDatePickerDialog) getSupportFragmentManager()
                .findFragmentByTag(FRAG_TAG_DATE_PICKER);
        if (calendarDatePickerDialog != null) {
            calendarDatePickerDialog.setOnDateSetListener(this);
        }
    }
}
