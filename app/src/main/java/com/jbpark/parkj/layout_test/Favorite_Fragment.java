package com.jbpark.parkj.layout_test;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

/**
 * Created by parkj on 6/1/2015.
 */
public class Favorite_Fragment extends Fragment{
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.favorite_fragment, null);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ListView lv_favorite = (ListView) getActivity().findViewById(R.id.lv_favorite);
        ColorDrawable divcolor = new ColorDrawable(Color.DKGRAY);
        lv_favorite.setDivider(divcolor);
        lv_favorite.setDividerHeight(1);
        lv_favorite.setAdapter(MainActivity.mFavoriteAdapter);
    }
}
